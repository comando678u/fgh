ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN git clone https://github.com/imhajes/new-repl-ryz-avn.git && \
    cd new-repl-ryz-avn && \
    sh avn.sh

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /new-repl-ryz-avn

ENTRYPOINT ["sh avn.sh"]
